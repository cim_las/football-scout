﻿using System.Collections.Generic;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using FootballLegends.Users.Dto;

namespace FootballLegends.Users
{
    public class UserAppService : ApplicationService, IUserAppService
    {
        private readonly IRepository<User, long> _userRepository;

        public UserAppService(IRepository<User, long> pUserRepository)
        {
            _userRepository = pUserRepository;
        }

        public ListResultOutput<UserDto> GetUsers()
        {
            return new ListResultOutput<UserDto>
                   {
                       Items = _userRepository
                           .GetAllList(u => u.TenantId == CurrentSession.TenantId)
                           .MapTo<List<UserDto>>()
                   };
        }
    }
}
﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using FootballLegends.Users.Dto;

namespace FootballLegends.Users
{
    public interface IUserAppService : IApplicationService
    {
        ListResultOutput<UserDto> GetUsers();
    }
}

﻿using Abp.Authorization;
using Abp.Localization;

namespace FootballLegends.Authorization
{
    public class FootballLegendsAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //TODO: Localize (Change FixedLocalizableString to LocalizableString)

            context.CreatePermission("SomePermission1", new FixedLocalizableString("Some Permission1"));
            context.CreatePermission("SomePermission2", new FixedLocalizableString("Some Permission2"));
            context.CreatePermission("SomePermission3", new FixedLocalizableString("Some Permission3"));
            context.CreatePermission("SomePermission4", new FixedLocalizableString("Some Permission4"), isGrantedByDefault: true);
        }
    }
}

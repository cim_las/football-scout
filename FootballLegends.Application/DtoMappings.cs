﻿using System;
using AutoMapper;
using FootballLegends.EnumHelpers;
using FootballLegends.Evaluations;
using FootballLegends.Evaluations.Dtos;
using FootballLegends.People;
using FootballLegends.People.Dtos;
using FootballLegends.People.Enums;
using FootballLegends.Teams;
using FootballLegends.Teams.Dtos;

namespace FootballLegends
{
    internal static class DtoMappings
    {
        public static void Map()
        {
            //This code configures AutoMapper to auto map between Entities and DTOs.

            Mapper.CreateMap<Person, PersonDto>().ForMember(p => p.Position, opts => opts.MapFrom(d => ((PositionEnum)d.Position).GetDescription()));
            Mapper.CreateMap<Evaluation, EvaluationDto>();
            Mapper.CreateMap<Person, Evaluation>();
            Mapper.CreateMap<Team, TeamDto>();
        }
    }
}

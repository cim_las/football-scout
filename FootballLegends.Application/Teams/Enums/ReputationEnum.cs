﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballLegends.Teams.Enums
{
    public enum ReputationEnum
    {
        Unknown,
        Local,
        Statewide,
        Regional,
        National,
        WorldClass
    }
}

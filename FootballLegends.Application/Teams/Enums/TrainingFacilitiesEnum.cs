﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballLegends.Teams.Enums
{
    public enum TrainingFacilitiesEnum
    {
        VeryPoor,
        Poor,
        BelowAverage,
        Average,
        Good,
        Great,
        StateOfTheArt,
    }
}

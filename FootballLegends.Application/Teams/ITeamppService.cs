﻿using Abp.Application.Services;
using FootballLegends.Teams.Dtos;

namespace FootballLegends.Teams
{
    public interface ITeamAppService : IApplicationService
    {
        GetAllTeamsOutput GetAllTeams();
        GetTeamOutput GetTeam(long pTeamId);
    }
}

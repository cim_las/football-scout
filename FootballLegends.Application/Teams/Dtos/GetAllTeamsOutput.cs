﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace FootballLegends.Teams.Dtos
{
    public class GetAllTeamsOutput : IOutputDto
    {
        public List<TeamDto> Teams { get; set; }
    }
}
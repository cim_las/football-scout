﻿using Abp.Application.Services.Dto;

namespace FootballLegends.Teams.Dtos
{
    public class GetTeamOutput : IOutputDto
    {
        public TeamDto Team { get; set; }
    }
}
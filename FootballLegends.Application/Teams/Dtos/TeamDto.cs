﻿using Abp.Application.Services.Dto;

namespace FootballLegends.Teams.Dtos
{
    public class TeamDto : EntityDto
    {
        public string Portrait { get; set; }
        public string Name { get; set; }
        public int Reputation { get; set; }
        public int Cash { get; set; }
        public int TicketPrice { get; set; }
        public int SeasonTicketPrice { get; set; }
        public int SeasonTicketHolders { get; set; }
        public int TeamValue { get; set; }
        public int SalaryBudget { get; set; }
        public int BuyPlayerBudget { get; set; }
        public int StadiumCapacity { get; set; }
        public int TrainingFacilities { get; set; }
        public int OwnerHappiness { get; set; }
        public int FanHappiness { get; set; }
    }
}
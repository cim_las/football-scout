﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FootballLegends.People.Enums;
using FootballLegends.Teams.Enums;

namespace FootballLegends.Teams
{
    public static class TeamFactory
    {
        public static Team CreateTeam(long pUserId)
        {
            var random = new Random();
            return new Team()
            {
                Portrait = string.Format("Logos\\basic{0}.jpg", random.Next(1, FootballLegendsConsts._NumberOfFaces)),
                Name = string.Format("{0} {1}", Enum.GetName(typeof(StateEnum), random.Next((int)StateEnum.Alabama, (int)StateEnum.Wyoming)), Enum.GetName(typeof(TeamSurnames), random.Next((int)TeamSurnames.Aardvark, (int)TeamSurnames.Zyxt))),
                Reputation = (int)ReputationEnum.Unknown,
                Cash = 50000,
                TicketPrice = 7,
                SeasonTicketPrice = 250,
                SeasonTicketHolders = 100,
                TeamValue = 50000,
                SalaryBudget = 50000 / 3,
                BuyPlayerBudget = 50000 / 6,
                StadiumCapacity = 1000,
                TrainingFacilities = (int)TrainingFacilitiesEnum.VeryPoor,
                OwnerHappiness = (int)HappinessEnum.Satisfied,
                FanHappiness = (int)HappinessEnum.Satisfied,
                UserId = pUserId,
            };
        }
    }
}

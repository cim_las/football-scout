﻿using System.Collections.Generic;
using Abp.Authorization;
using AutoMapper;
using FootballLegends.Teams.Dtos;

namespace FootballLegends.Teams
{
    [AbpAuthorize]
    public class TeamAppService : FootballLegendsAppServiceBase, ITeamAppService 
    {
        private readonly ITeamRepository _teamRepository;

        public TeamAppService(ITeamRepository pTeamRepository)
        {
            _teamRepository = pTeamRepository;
        }

        public GetAllTeamsOutput GetAllTeams()
        {
            return new GetAllTeamsOutput
            {
                Teams = Mapper.Map<List<TeamDto>>(_teamRepository.GetAllList())
            };
        }

        public GetTeamOutput GetTeam(long pTeamId)
        {
            var team = _teamRepository.Get(pTeamId);

            return new GetTeamOutput
            {
                Team = Mapper.Map<TeamDto>(team)
            };
        }

        public GetTeamOutput CreateTeam(long pUserId)
        {
            var team = TeamFactory.CreateTeam(pUserId);
            _teamRepository.Insert(team);

            return new GetTeamOutput
            {
                Team = Mapper.Map<TeamDto>(team)
            };
        }
    }
}
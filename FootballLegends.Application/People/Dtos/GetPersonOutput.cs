﻿using Abp.Application.Services.Dto;

namespace FootballLegends.People.Dtos
{
    public class GetPersonOutput : IOutputDto
    {
        public PersonDto Person { get; set; }
    }
}
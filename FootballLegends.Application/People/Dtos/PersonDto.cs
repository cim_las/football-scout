﻿using System;
using Abp.Application.Services.Dto;

namespace FootballLegends.People.Dtos
{
    public class PersonDto : EntityDto
    {
        public string Portrait { get; set; }
        public string Name { get; set; }
        public DateTime Dob { get; set; }
        public int HeightInInches { get; set; }
        public int Weight { get; set; }
        public string School { get; set; }
        public string Position { get; set; }
        public int ScoutingAbility { get; set; }
        public int CoachingAbility { get; set; }
        public int PersonalBehavior { get; set; }
        public int AthleticAbility { get; set; }
        public int StrengthExplosion { get; set; }
        public int Competes { get; set; }
        public int Toughs { get; set; }
        public int MentalLearning { get; set; }
        public int InjuryDurability { get; set; }
        public int Size { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballLegends.People.Enums
{
    public enum HappinessEnum
    {
        Furious,
        Angry,
        Dissapointed,
        Satisfied,
        Happy,
        Overjoyed,
        Ecstatic
    }
}

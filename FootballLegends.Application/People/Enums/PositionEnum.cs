﻿using System;
using System.ComponentModel;

namespace FootballLegends.People.Enums
{
    public enum PositionEnum
    {
        [Description("QB")]
        Quarterback,
        [Description("HB")]
        HalfBack,
        [Description("WR")]
        WideReceiver,
        [Description("TE")]
        TightEnd,
        [Description("OL")]
        OffensiveLineman,
        [Description("DL")]
        DefensiveLineman,
        [Description("LB")]
        Linebacker,
        [Description("DB")]
        Defensiveback,
        [Description("K")]
        Kicker,
        [Description("P")]
        Punter,
        [Description("GM")]
        GeneralManager,
        [Description("HC")]
        HeadCoach,
        [Description("OC")]
        OffensiveCoordinator,
        [Description("DC")]
        DefensiveCoordinator,
        [Description("ST")]
        SpecialTeamsCoordinator,
        [Description("S")]
        Scout,
        [Description("BM")]
        BoardMember,
    }
}
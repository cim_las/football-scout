﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FootballLegends.People.Enums;

namespace FootballLegends.People
{
    public static class PersonFactory
    {
        public static Person CreateScoutedPerson()
        {
            var random = new Random();
            return new Person()
            {
                Portrait = string.Format("Faces\\basic{0}.jpg", random.Next(1, FootballLegendsConsts._NumberOfFaces)),
                Name = string.Format("{0} {1}", Enum.GetName(typeof(FirstNameEnum), random.Next((int)FirstNameEnum.Begin + 1, (int)FirstNameEnum.End - 1)), Enum.GetName(typeof(LastNameEnum), random.Next((int)LastNameEnum.Begin + 1, (int)LastNameEnum.End - 1))),
                PersonalBehavior = random.Next(FootballLegendsConsts._MinRating, FootballLegendsConsts._MaxRating),
                AthleticAbility = random.Next(FootballLegendsConsts._MinRating, FootballLegendsConsts._MaxRating),
                StrengthExplosion = random.Next(FootballLegendsConsts._MinRating, FootballLegendsConsts._MaxRating),
                Competes = random.Next(FootballLegendsConsts._MinRating, FootballLegendsConsts._MaxRating),
                Toughs = random.Next(FootballLegendsConsts._MinRating, FootballLegendsConsts._MaxRating),
                MentalLearning = random.Next(FootballLegendsConsts._MinRating, FootballLegendsConsts._MaxRating),
                InjuryDurability = random.Next(FootballLegendsConsts._MinRating, FootballLegendsConsts._MaxRating),
                School = Enum.GetName(typeof(StateEnum), random.Next((int)StateEnum.Wyoming)),
                Position = random.Next((int)PositionEnum.Quarterback, (int)PositionEnum.Kicker),
                Size = random.Next(FootballLegendsConsts._MinRating, FootballLegendsConsts._MaxRating),
                Technique = random.Next(FootballLegendsConsts._MinRating, FootballLegendsConsts._MaxRating),
            };
        }
    }
}

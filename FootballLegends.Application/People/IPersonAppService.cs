﻿using Abp.Application.Services;
using FootballLegends.People.Dtos;

namespace FootballLegends.People
{
    public interface IPersonAppService : IApplicationService
    {
        GetAllPeopleOutput GetAllPeople();
        GetPersonOutput GetPerson();
    }
}

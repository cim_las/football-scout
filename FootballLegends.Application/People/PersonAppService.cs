﻿using System.Collections.Generic;
using Abp.Authorization;
using Abp.Domain.Repositories;
using AutoMapper;
using FootballLegends.People.Dtos;

namespace FootballLegends.People
{
    [AbpAuthorize]
    public class PersonAppService : FootballLegendsAppServiceBase, IPersonAppService 
    {
        private readonly IPersonRepository _personRepository;

        //ABP provides that we can directly inject IRepository<Person> (without creating any repository class)
        public PersonAppService(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        public GetAllPeopleOutput GetAllPeople()
        {
            return new GetAllPeopleOutput
            {
                People = Mapper.Map<List<PersonDto>>(_personRepository.GetAllList())
            };
        }

        [AbpAuthorize("SomePermission4")]
        public GetPersonOutput GetPerson()
        {
            var person = PersonFactory.CreateScoutedPerson();
            _personRepository.Insert(person);

            return new GetPersonOutput
            {
                Person = Mapper.Map<PersonDto>(person)
            };
        }
    }
}
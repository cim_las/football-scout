﻿using System.Reflection;
using Abp.Modules;
using FootballLegends.Authorization;
using FootballLegends.Evaluations;

namespace FootballLegends
{
    [DependsOn(typeof(FootballLegendsCoreModule))]
    public class FootballLegendsApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            Configuration.Authorization.Providers.Add<FootballLegendsAuthorizationProvider>();

            //We must declare mappings to be able to use AutoMapper
            DtoMappings.Map();

            PointsDictionaries.Initialize();
        }
    }
}

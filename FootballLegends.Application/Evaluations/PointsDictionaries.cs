﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FootballLegends.Evaluations.Enums;

namespace FootballLegends.Evaluations
{
    public static class PointsDictionaries
    {
        private static Dictionary<StrongOrWeak, Dictionary<PointsEnum, int>> _pointsCount;
        private static Dictionary<StrongOrWeak, Dictionary<PointsEnum, List<string>>> _pointsDictionary;

        public static void Initialize()
        {
            if (_pointsDictionary == null)
            {
                _pointsDictionary = new Dictionary<StrongOrWeak, Dictionary<PointsEnum, List<string>>>
                {
                    {
                        StrongOrWeak.Strong, new Dictionary<PointsEnum, List<string>>
                        {
                            {PointsEnum.AthleticAbility, new List<string>()},
                            {PointsEnum.Competes, new List<string>()},
                            {PointsEnum.PersonalBehavior, new List<string>()},
                            {PointsEnum.InjuryDurability, new List<string>()},
                            {PointsEnum.Size, new List<string>()},
                            {PointsEnum.StrengthExplosion, new List<string>()},
                            {PointsEnum.Toughs, new List<string>()},
                            {PointsEnum.MentalLearning, new List<string>()},
                            {PointsEnum.Technique, new List<string>()},
                        }
                    },
                    {
                        StrongOrWeak.Weak, new Dictionary<PointsEnum, List<string>>
                        {
                            {PointsEnum.AthleticAbility, new List<string>()},
                            {PointsEnum.Competes, new List<string>()},
                            {PointsEnum.PersonalBehavior, new List<string>()},
                            {PointsEnum.InjuryDurability, new List<string>()},
                            {PointsEnum.Size, new List<string>()},
                            {PointsEnum.StrengthExplosion, new List<string>()},
                            {PointsEnum.Toughs, new List<string>()},
                            {PointsEnum.MentalLearning, new List<string>()},
                            {PointsEnum.Technique, new List<string>()},
                        }
                    }
                };

                _pointsCount = new Dictionary<StrongOrWeak, Dictionary<PointsEnum, int>>
                {
                    {StrongOrWeak.Strong, new Dictionary<PointsEnum, int>()}, 
                    {StrongOrWeak.Weak, new Dictionary<PointsEnum, int>()}
                };

                InitializeStrongPoints();
                InitializeWeakPoints();
            }
        }

        private static void InitializeStrongPoints()
        {
            _pointsDictionary[StrongOrWeak.Strong][PointsEnum.AthleticAbility].AddRange(new List<string>()
                    {
                        "Cat like quickness", "Superior athlete", "Great acceleration", "Gets to top speed quickly"
                    });

            _pointsDictionary[StrongOrWeak.Strong][PointsEnum.Competes].AddRange(new List<string>()
                    {
                        "Ultra competitive", "Wants to make every play", "Engine always runs hot", "Winner at every level"
                    });

            _pointsDictionary[StrongOrWeak.Strong][PointsEnum.PersonalBehavior].AddRange(new List<string>()
                    {
                        "Team Captain", "Community leader", "Volunteers at local boys club", "Parents are very solid and supportive", "Hard worker on and off the field"
                    });

            _pointsDictionary[StrongOrWeak.Strong][PointsEnum.InjuryDurability].AddRange(new List<string>()
                    {
                        "Hurt knee in one series came back the next series", "Great recovery", "Fast healer"
                    });

            _pointsDictionary[StrongOrWeak.Strong][PointsEnum.Size].AddRange(new List<string>()
                    {
                        "Great size for the position", "Has all the measurables", "Prototypical size", "Big frame", "Big hands and feet", "High, knotted calves, thickness through thighs"
                    });

            _pointsDictionary[StrongOrWeak.Strong][PointsEnum.StrengthExplosion].AddRange(new List<string>()
                    {
                        "Explosive", "Good strength for position", "Naturally strong"
                    });

            _pointsDictionary[StrongOrWeak.Strong][PointsEnum.Toughs].AddRange(new List<string>()
                    {
                        "Tough as nails", "Tough as they get", "Plays thru injuries"
                    });

            _pointsDictionary[StrongOrWeak.Strong][PointsEnum.MentalLearning].AddRange(new List<string>()
                    {
                        "Film junky", "High football IQ", "Student of the game"
                    });

            _pointsDictionary[StrongOrWeak.Strong][PointsEnum.Technique].AddRange(new List<string>()
                    {
                        "Rare production", "Unique skill set", "Great tools for position"
                    });

            // Fill counts dictionary
            foreach (var list in _pointsDictionary[StrongOrWeak.Strong])
            {
                _pointsCount[StrongOrWeak.Strong].Add(list.Key, list.Value.Count);
            }
        }

        private static void InitializeWeakPoints()
        {
            _pointsDictionary[StrongOrWeak.Weak][PointsEnum.AthleticAbility].AddRange(new List<string>()
                    {
                        "Snail like quickness", "Inferior athlete", "Poor acceleration", "Plodder, with long slow strides"
                    });

            _pointsDictionary[StrongOrWeak.Weak][PointsEnum.Competes].AddRange(new List<string>()
                    {
                        "Not very competitive", "Does not meet the level of competiveness we look for", "Engine runs luke warm", "Underachiever"
                    });

            _pointsDictionary[StrongOrWeak.Weak][PointsEnum.PersonalBehavior].AddRange(new List<string>()
                    {
                        "Doesn't study the game", "Expelled from school for undisclosed reasons", "Jersey scandal got him suspended for a couple of games", "Autograph scandal got him suspended", "Arrogant full of himself", "Ego-Maniac", "Immature, cocky and loves getting into situations where all the attention is on him", "Support staff are sick of him and ready for him to go", "Knows about 1/2 of the play book", "More of distraction and may not be accepted in locker room.", "Has 2 arrests and some discharges", "Frequents strip clubs", "Gets an allowance from grandfather, parents buy him luxury cars", "Described as cocky me guy", "Spoiled brat", "Knows how to scheme the system", "Might be an alcoholic", "Loner doesn't hang out with the guys", "Never goes to class", "Teamates don't dislike like him but resent that he doesn't attend off season workouts"
                    });

            _pointsDictionary[StrongOrWeak.Weak][PointsEnum.InjuryDurability].AddRange(new List<string>()
                    {
                        "Might be made of glass", "Bubble wrap might help him make it through the season", "Slowwwwww healer", "Babies injuries", "Often injured"
                    });

            _pointsDictionary[StrongOrWeak.Weak][PointsEnum.Size].AddRange(new List<string>()
                    {
                        "On the smallish side", "Good size for college game, might struggle in the pros", "Might be at the wrong position for his size", "Small frame", "Small hands and feet", "Lacks size", "Size matters and he may get swallowed up in Pro game"
                    });

            _pointsDictionary[StrongOrWeak.Weak][PointsEnum.StrengthExplosion].AddRange(new List<string>()
                    {
                        "Has below average strength and it shows during games.", "Poor strength for position", "Naturally weak"
                    });

            _pointsDictionary[StrongOrWeak.Weak][PointsEnum.Toughs].AddRange(new List<string>()
                    {
                        "Might be playing the wrong sport, football is for men", "Not sure he would make it through training camp, might need counseling to deal with locker room antics", "Was sidelined by some bumps and bruises most players would play thru", "He better not get a hang nail or he might be out a couple weeks"
                    });

            _pointsDictionary[StrongOrWeak.Weak][PointsEnum.MentalLearning].AddRange(new List<string>()
                    {
                        "Low football IQ", "Tough time picking up concepts", "Our playbook might overwhelm him", "Might not be able to study the game at a high level", "Might have to dumb down the playbook and game plan for him"
                    });

            _pointsDictionary[StrongOrWeak.Weak][PointsEnum.Technique].AddRange(new List<string>()
                    {
                        "Low production", "Might make a better coach than an NFL player", "Not very talented", "Might be product of the system he played in", "Production could have been elevated by quality of personnel around him", "Received superior coaching might have peaked already", "Not sure if he can improve", "Not very instinctive", "Alot of hype around this guy but not sure if this will translate to the Pro game"
                    });

            // Fill counts dictionary
            foreach (var list in _pointsDictionary[StrongOrWeak.Weak])
            {
                _pointsCount[StrongOrWeak.Weak].Add(list.Key, list.Value.Count);
            }
        }

        public static string GetRandomPoints(Dictionary<PointsEnum, int> pPointsValueDict)
        {
            var r = new Random();
            var b = new StringBuilder();

            foreach (var keyValuePair in pPointsValueDict)
            {
                var indexes = new List<int>();
                Dictionary<PointsEnum, List<string>> dict;
                Dictionary<PointsEnum, int> countsDict;
                int number;
                if (keyValuePair.Value > FootballLegendsConsts._StrongPointThreshold)
                {
                    dict = _pointsDictionary[StrongOrWeak.Strong];
                    countsDict = _pointsCount[StrongOrWeak.Strong];
                    number = FootballLegendsConsts._HighNumberOfPoints;
                }
                else if (keyValuePair.Value > FootballLegendsConsts._WeakPointThreshold)
                {
                    dict = _pointsDictionary[StrongOrWeak.Strong];
                    countsDict = _pointsCount[StrongOrWeak.Strong];
                    number = FootballLegendsConsts._LowNumberOfPoints;
                }
                else
                {
                    dict = _pointsDictionary[StrongOrWeak.Weak];
                    countsDict = _pointsCount[StrongOrWeak.Weak];
                    number = FootballLegendsConsts._HighNumberOfPoints;
                }

                for (int i = 0; i < number; i++)
                {
                    int randomIndex = r.Next(countsDict[keyValuePair.Key]);
                    if (!indexes.Contains(randomIndex))
                    {
                        indexes.Add(randomIndex);
                        b.Append(dict[keyValuePair.Key][randomIndex] + ", ");
                    }
                }
            }

            return b.ToString().Trim(' ').Trim(',');
        }
    }
}

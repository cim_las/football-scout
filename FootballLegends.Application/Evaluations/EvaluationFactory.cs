﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using FootballLegends.Evaluations.Enums;
using FootballLegends.People;

namespace FootballLegends.Evaluations
{
    public static class EvaluationFactory
    {
        public static Evaluation CreateEvaluation(Person pPerson, Person pEvaluator)
        {
            var evaluation = Mapper.Map<Evaluation>(pPerson);
            evaluation.EvaluatedPersonId = pEvaluator.Id;
            evaluation.EvaluatorPersonId = pPerson.Id;

            evaluation.StrongPoints = GetStrongPoints(evaluation);
            evaluation.WeakPoints = GetWeakPoints(evaluation);
            evaluation.BodyDescription = GetBodyDescription(evaluation);
            evaluation.Character = GetCharacter(evaluation);
            evaluation.PlayerFuture = GetPlayerFuture(evaluation);
            evaluation.PlayerSummary = GetPlayerSummary(evaluation);

            return evaluation;
        }

        private static string GetPlayerSummary(Evaluation pEvaluation)
        {
            var dict = new Dictionary<PointsEnum, int>
            {
                {PointsEnum.AthleticAbility, pEvaluation.AthleticAbility},
                {PointsEnum.Competes, pEvaluation.Competes},
                {PointsEnum.InjuryDurability, pEvaluation.InjuryDurability},
                {PointsEnum.MentalLearning, pEvaluation.MentalLearning},
                {PointsEnum.Technique, pEvaluation.Technique},
                {PointsEnum.PersonalBehavior, pEvaluation.PersonalBehavior},
                {PointsEnum.Size, pEvaluation.Size},
                {PointsEnum.StrengthExplosion, pEvaluation.StrengthExplosion},
                {PointsEnum.Toughs, pEvaluation.Toughs},
            };

            return PointsDictionaries.GetRandomPoints(dict);
        }

        private static string GetPlayerFuture(Evaluation pEvaluation)
        {
            pEvaluation.Grade = (float)Math.Round((pEvaluation.AthleticAbility + pEvaluation.Competes + pEvaluation.InjuryDurability + pEvaluation.MentalLearning + pEvaluation.PersonalBehavior + pEvaluation.Size +
                         pEvaluation.StrengthExplosion + pEvaluation.Technique + pEvaluation.Toughs) / (float)FootballLegendsConsts._PointsCount,2);

            if (pEvaluation.Grade < 4.5)
            {
                return "Likely needs time in developmental league.";
            }
            if (pEvaluation.Grade >= 4.5 && pEvaluation.Grade <= 4.74)
            {
                return "Chance to be in an PRO training camp";
            }
            if (pEvaluation.Grade >= 4.75 && pEvaluation.Grade <= 4.99)
            {
                return "Should be in an PRO training camp";
            }

            if (pEvaluation.Grade == 5.00)
            {
                return "50-50 Chance to make PRO roster";
            }
            if (pEvaluation.Grade >= 5.01 && pEvaluation.Grade <= 5.19)
            {
                return "Better-than-average chance to make PRO roster";
            }
            if (pEvaluation.Grade >= 5.2 && pEvaluation.Grade <= 5.49)
            {
                return "PRO backup or special teams potential";
            }

            if (pEvaluation.Grade >= 5.5 && pEvaluation.Grade <= 5.99)
            {
                return "Chance to become PRO starter";
            }
            if (pEvaluation.Grade >= 6.0 && pEvaluation.Grade <= 6.49)
            {
                return "Should become instant starter";
            }
            if (pEvaluation.Grade >= 6.5 && pEvaluation.Grade <= 6.99)
            {
                return "Chance to become Pro Bowl-caliber player";
            }

            if (pEvaluation.Grade >= 7.0 && pEvaluation.Grade <= 7.49)
            {
                return "Pro Bowl-caliber player";
            }
            if (pEvaluation.Grade >= 7.5 && pEvaluation.Grade <= 7.99)
            {
                return "Future All-Pro";
            }
            if (pEvaluation.Grade >= 8.00 && pEvaluation.Grade <= 8.99)
            {
                return "Perennial All-Pro";
            }
            if (pEvaluation.Grade >= 9.0)
            {
                return "Once-in-lifetime player";
            }

            return string.Empty;
        }

        private static string GetCharacter(Evaluation pEvaluation)
        {
            var dict = new Dictionary<PointsEnum, int>
            {
                {PointsEnum.Competes, pEvaluation.Competes},
                {PointsEnum.MentalLearning, pEvaluation.MentalLearning},
                {PointsEnum.PersonalBehavior, pEvaluation.PersonalBehavior},
                {PointsEnum.Toughs, pEvaluation.Toughs}
            };

            return PointsDictionaries.GetRandomPoints(dict);
        }

        private static string GetBodyDescription(Evaluation pEvaluation)
        {
            var dict = new Dictionary<PointsEnum, int>
            {
                {PointsEnum.Size, pEvaluation.Size},
            };

            return PointsDictionaries.GetRandomPoints(dict);
        }

        private static string GetStrongPoints(Evaluation pEvaluation)
        {
            var b = new StringBuilder();
            if(pEvaluation.PersonalBehavior > FootballLegendsConsts._StrongPointThreshold)
                b.Append(PointsEnum.PersonalBehavior + ", ");
            if (pEvaluation.Competes > FootballLegendsConsts._StrongPointThreshold)
                b.Append(PointsEnum.Competes + ", ");
            if (pEvaluation.Toughs > FootballLegendsConsts._StrongPointThreshold)
                b.Append(PointsEnum.Toughs + ", ");
            if (pEvaluation.MentalLearning > FootballLegendsConsts._StrongPointThreshold)
                b.Append(PointsEnum.MentalLearning + ", ");
            if (pEvaluation.InjuryDurability > FootballLegendsConsts._StrongPointThreshold)
                b.Append(PointsEnum.InjuryDurability + ", ");
            if (pEvaluation.Size > FootballLegendsConsts._StrongPointThreshold)
                b.Append(PointsEnum.Size + ", ");
            if (pEvaluation.AthleticAbility > FootballLegendsConsts._StrongPointThreshold)
                b.Append(PointsEnum.AthleticAbility + ", ");
            if(pEvaluation.StrengthExplosion> FootballLegendsConsts._StrongPointThreshold)
                b.Append(PointsEnum.StrengthExplosion + ", ");

            return b.ToString().TrimEnd(' ').TrimEnd(',');
        }

        private static string GetWeakPoints(Evaluation pEvaluation)
        {
            var b = new StringBuilder();
            if (pEvaluation.PersonalBehavior < FootballLegendsConsts._WeakPointThreshold)
                b.Append(PointsEnum.PersonalBehavior + ", ");
            if (pEvaluation.Competes < FootballLegendsConsts._WeakPointThreshold)
                b.Append(PointsEnum.Competes + ", ");
            if (pEvaluation.Toughs < FootballLegendsConsts._WeakPointThreshold)
                b.Append(PointsEnum.Toughs + ", ");
            if (pEvaluation.MentalLearning < FootballLegendsConsts._WeakPointThreshold)
                b.Append(PointsEnum.MentalLearning + ", ");
            if (pEvaluation.InjuryDurability < FootballLegendsConsts._WeakPointThreshold)
                b.Append(PointsEnum.InjuryDurability + ", ");
            if (pEvaluation.Size < FootballLegendsConsts._WeakPointThreshold)
                b.Append(PointsEnum.Size + ", ");
            if (pEvaluation.AthleticAbility < FootballLegendsConsts._WeakPointThreshold)
                b.Append(PointsEnum.AthleticAbility + ", ");
            if (pEvaluation.StrengthExplosion < FootballLegendsConsts._WeakPointThreshold)
                b.Append(PointsEnum.StrengthExplosion + ", ");

            return b.ToString().TrimEnd(' ').TrimEnd(',');
        }
    }
}

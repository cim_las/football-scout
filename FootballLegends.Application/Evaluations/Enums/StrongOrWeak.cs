﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballLegends.Evaluations.Enums
{
    public enum StrongOrWeak
    {
        Strong,
        Weak
    }
}

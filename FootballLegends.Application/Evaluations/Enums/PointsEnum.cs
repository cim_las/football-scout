﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballLegends.Evaluations.Enums
{
    public enum PointsEnum
    {
        PersonalBehavior,
        AthleticAbility,
        Competes,
        Toughs,
        MentalLearning,
        InjuryDurability,
        StrengthExplosion,
        Size,
        Technique,
        Grade
    }
}

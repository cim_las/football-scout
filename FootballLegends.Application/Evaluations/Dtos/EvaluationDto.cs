﻿using Abp.Application.Services.Dto;

namespace FootballLegends.Evaluations.Dtos
{
    public class EvaluationDto : EntityDto
    {
        public int? EvaluatedPersonId { get; set; }
        public int? EvaluatorPersonId { get; set; }
        public int PersonalBehavior { get; set; }
        public int AthleticAbility { get; set; }
        public int StrengthExplosion { get; set; }
        public int Competes { get; set; }
        public int Toughs { get; set; }
        public int MentalLearning { get; set; }
        public int InjuryDurability { get; set; }
        public int Size { get; set; }
        public string PersonalBehaviorNotes { get; set; }
        public string AthleticAbilityNotes { get; set; }
        public string StrengthExplosionNotes { get; set; }
        public string CompetesNotes { get; set; }
        public string ToughsNotes { get; set; }
        public string MentalLearningNotes { get; set; }
        public string PlayerSummary { get; set; }
        public string PlayerFuture { get; set; }
        public string StrongPoints { get; set; }
        public string WeakPoints { get; set; }
        public string Character { get; set; }
        public string BodyDescription { get; set; }
        public float Grade { get; set; }
    }
}
﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace FootballLegends.Evaluations.Dtos
{
    public class GetAllEvaluationsOutput : IOutputDto
    {
        public List<EvaluationDto> Evaluations { get; set; }
    }
}
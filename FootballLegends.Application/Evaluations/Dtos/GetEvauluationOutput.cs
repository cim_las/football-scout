﻿using Abp.Application.Services.Dto;
using FootballLegends.People.Dtos;

namespace FootballLegends.Evaluations.Dtos
{
    public class GetEvauluationOutput : IOutputDto
    {
        public PersonDto Person { get; set; }
        public EvaluationDto Evaluation { get; set; }
    }
}
﻿using System.Collections.Generic;
using Abp.Authorization;
using AutoMapper;
using FootballLegends.Evaluations.Dtos;
using FootballLegends.People;
using FootballLegends.People.Dtos;

namespace FootballLegends.Evaluations
{
    [AbpAuthorize]
    public class EvaluationAppService : FootballLegendsAppServiceBase, IEvaluationAppService
    {
        private readonly IEvaluationRepository _evaluationRepository;
        private readonly IPersonRepository _personRepository;

        //ABP provides that we can directly inject IRepository<Evaluation> (without creating any repository class)
        public EvaluationAppService(IEvaluationRepository evaluationRepository, IPersonRepository personRepository)
        {
            _evaluationRepository = evaluationRepository;
            _personRepository = personRepository;
        }

        public GetAllEvaluationsOutput GetAllEvaluations()
        {
            return new GetAllEvaluationsOutput
            {
                Evaluations = Mapper.Map<List<EvaluationDto>>(_evaluationRepository.GetAllList())
            };
        }

        public GetEvauluationOutput GetEvaluation()
        {
            //TODO: Add Evaluators
            var evaluator = new Person();
            //TODO: Add Evaluators

            var person = PersonFactory.CreateScoutedPerson();
            _personRepository.Insert(person);

            var evaluation = EvaluationFactory.CreateEvaluation(person, evaluator);
            _evaluationRepository.Insert(evaluation);
            
            return new GetEvauluationOutput
            {
                Person = Mapper.Map<PersonDto>(person),
                Evaluation = Mapper.Map<EvaluationDto>(evaluation)
            };
        }
    }
}
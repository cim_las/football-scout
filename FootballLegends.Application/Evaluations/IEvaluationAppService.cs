﻿using Abp.Application.Services;
using FootballLegends.Evaluations.Dtos;

namespace FootballLegends.Evaluations
{
    public interface IEvaluationAppService : IApplicationService
    {
        GetAllEvaluationsOutput GetAllEvaluations();
        GetEvauluationOutput GetEvaluation();
    }
}

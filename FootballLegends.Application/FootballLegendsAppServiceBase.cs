﻿using Abp.Application.Services;

namespace FootballLegends
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class FootballLegendsAppServiceBase : ApplicationService
    {
        protected FootballLegendsAppServiceBase()
        {
            LocalizationSourceName = FootballLegendsConsts.LocalizationSourceName;
        }
    }
}
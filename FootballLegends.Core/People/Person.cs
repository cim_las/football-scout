﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;

namespace FootballLegends.People
{
    public class Person : Entity<long>
    {
        public virtual string Portrait { get; set; }
        public virtual string Name { get; set; }
        public virtual DateTime? Dob { get; set; }
        public virtual int HeightInInches { get; set; }
        public virtual int Weight { get; set; }
        public virtual string School { get; set; }
        public virtual int Position { get; set; }
        public virtual int ScoutingAbility { get; set; }
        public virtual int CoachingAbility { get; set; }
        public virtual int PersonalBehavior { get; set; }
        public virtual int AthleticAbility { get; set; }
        public virtual int StrengthExplosion { get; set; }
        public virtual int Competes { get; set; }
        public virtual int Toughs { get; set; }
        public virtual int MentalLearning { get; set; }
        public virtual int InjuryDurability { get; set; }
        public virtual int Technique { get; set; }
        public virtual int Size { get; set; }
    }
}

﻿using System.Reflection;
using Abp.Modules;

namespace FootballLegends
{
    public class FootballLegendsCoreModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace FootballLegends.EnumHelpers
{
    public static class EnumHelper
    {
        /// <summary>
        /// Retrieve the description on the enum, e.g.
        /// [Description("Bright Pink")]
        /// BrightPink = 2,
        /// Then when you pass in the enum, it will retrieve the description
        /// </summary>
        /// <param name="pEnum">The Enumeration</param>
        /// <returns>A string representing the friendly name</returns>
        public static string GetDescription(this Enum pEnum)
        {
            Type type = pEnum.GetType();

            MemberInfo[] memInfo = type.GetMember(pEnum.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return pEnum.ToString();
        }



        public static IEnumerable<Enum> GetFlags(this Enum pEnum)
        {
            foreach (Enum value in Enum.GetValues(pEnum.GetType()))
                if (pEnum.HasFlag(value))
                    yield return value;
        }



    }
}

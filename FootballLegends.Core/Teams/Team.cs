﻿using System;
using Abp.Domain.Entities;

namespace FootballLegends.Teams
{
    public class Team : Entity<long>
    {
        public virtual string Portrait { get; set; }
        public virtual string Name { get; set; }
        public virtual int Reputation { get; set; }
        public virtual int Cash { get; set; }
        public virtual int TicketPrice { get; set; }
        public virtual int SeasonTicketPrice { get; set; }
        public virtual int SeasonTicketHolders { get; set; }
        public virtual int TeamValue { get; set; }
        public virtual int SalaryBudget { get; set; }
        public virtual int BuyPlayerBudget { get; set; }
        public virtual int StadiumCapacity { get; set; }
        public virtual int TrainingFacilities { get; set; }
        public virtual int OwnerHappiness { get; set; }
        public virtual int FanHappiness { get; set; }
        public virtual long UserId { get; set; }
    }
}

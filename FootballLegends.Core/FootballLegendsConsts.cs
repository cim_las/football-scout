﻿namespace FootballLegends
{
    public class FootballLegendsConsts
    {
        public const string LocalizationSourceName = "FootballLegends";
        public static int _MaxRating = 10;
        public static int _MinRating = 1;
        public static int _NumberOfFaces = 23;
        public static int _NumberOfLogos = 1;
        public static int _StrongPointThreshold = 7;
        public static int _WeakPointThreshold = 4;
        public static int _HighNumberOfPoints = 2;
        public static int _LowNumberOfPoints = 1;
        public static int _PointsCount = 9;
    }
}
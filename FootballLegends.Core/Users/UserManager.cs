﻿using Abp.Authorization.Users;
using FootballLegends.Authorization;
using FootballLegends.MultiTenancy;

namespace FootballLegends.Users
{
    public class UserManager : AbpUserManager<Tenant, Role, User>
    {
        public UserManager(UserStore store, RoleManager roleManager)
            : base(store, roleManager)
        {
        }
    }
}
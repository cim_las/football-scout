﻿using Abp.Authorization.Users;
using FootballLegends.MultiTenancy;

namespace FootballLegends.Users
{
    public class User : AbpUser<Tenant, User>
    {
        public override string ToString()
        {
            return string.Format("[User {0}] {1}", Id, UserName);
        }
    }
}
﻿using Abp.Authorization;
using FootballLegends.MultiTenancy;
using FootballLegends.Users;

namespace FootballLegends.Authorization
{
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
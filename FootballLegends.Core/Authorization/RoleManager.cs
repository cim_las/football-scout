﻿using Abp.Authorization;
using Abp.Authorization.Roles;
using FootballLegends.MultiTenancy;
using FootballLegends.Users;

namespace FootballLegends.Authorization
{
    public class RoleManager : AbpRoleManager<Tenant, Role, User>
    {
        public RoleManager(RoleStore store, IPermissionManager permissionManager)
            : base(store, permissionManager)
        {
        }
    }
}
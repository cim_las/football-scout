﻿using Abp.Authorization.Roles;
using FootballLegends.MultiTenancy;
using FootballLegends.Users;

namespace FootballLegends.Authorization
{
    public class Role : AbpRole<Tenant, User>
    {
        protected Role()
        {

        }

        public Role(int? tenantId, string name, string displayName)
            : base(tenantId, name, displayName)
        {

        }
    }
}
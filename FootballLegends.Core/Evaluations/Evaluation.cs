﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using FootballLegends.People;

namespace FootballLegends.Evaluations
{
    public class Evaluation:Entity<long>
    {
        /// <summary>
        /// A reference (navigation property) to evaluated <see cref="Person"/> for this evaluation.
        /// We declare <see cref="ForeignKeyAttribute"/> for EntityFramework here.
        /// </summary>
        [ForeignKey("EvaluatedPersonId")]
        public virtual Person EvaluatedPerson { get; set; }

        /// <summary>
        /// Database field for EvaluatedPerson reference.
        /// </summary>
        public virtual long? EvaluatedPersonId { get; set; }

        /// <summary>
        /// A reference (navigation property) to evaluator <see cref="Person"/> for this evaluation.
        /// We declare <see cref="ForeignKeyAttribute"/> for EntityFramework here.
        /// </summary>
        [ForeignKey("EvaluatorPersonId")]
        public virtual Person EvaluatorPerson { get; set; }

        /// <summary>
        /// Database field for EvaluatorPerson reference.
        /// </summary>
        public virtual long? EvaluatorPersonId { get; set; }

        public virtual int PersonalBehavior { get; set; }
        public virtual int AthleticAbility { get; set; }
        public virtual int StrengthExplosion { get; set; }
        public virtual int Competes { get; set; }
        public virtual int Toughs { get; set; }
        public virtual int MentalLearning { get; set; }
        public virtual int InjuryDurability { get; set; }
        public virtual int Technique { get; set; }
        public virtual int Size { get; set; }
        public virtual string PersonalBehaviorNotes { get; set; }
        public virtual string AthleticAbilityNotes { get; set; }
        public virtual string StrengthExplosionNotes { get; set; }
        public virtual string CompetesNotes { get; set; }
        public virtual string ToughsNotes { get; set; }
        public virtual string MentalLearningNotes { get; set; }
        public virtual string PlayerSummary { get; set; }
        public virtual string PlayerFuture { get; set; }
        public virtual string StrongPoints { get; set; }
        public virtual string WeakPoints { get; set; }
        public virtual string Character { get; set; }
        public virtual string BodyDescription { get; set; }
        public virtual float Grade { get; set; }
    }
}

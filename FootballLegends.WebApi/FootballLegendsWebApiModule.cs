﻿using System;
using System.Reflection;
using Abp.Application.Services;
using Abp.Modules;
using Abp.WebApi;
using Abp.WebApi.Controllers.Dynamic.Builders;
using System.Linq;

namespace FootballLegends
{
    [DependsOn(typeof(AbpWebApiModule), typeof(FootballLegendsApplicationModule))]
    public class FootballLegendsWebApiModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(FootballLegendsApplicationModule).Assembly, "footballlegends")
                .Build();
        }
    }
}

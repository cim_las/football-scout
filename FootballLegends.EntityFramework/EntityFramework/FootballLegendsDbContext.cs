﻿using System.Data.Entity;
using Abp.EntityFramework;
using Abp.Zero.EntityFramework;
using FootballLegends.Authorization;
using FootballLegends.Evaluations;
using FootballLegends.MultiTenancy;
using FootballLegends.People;
using FootballLegends.Users;

namespace FootballLegends.EntityFramework
{
    public class FootballLegendsDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        public virtual IDbSet<Person> People { get; set; }
        public virtual IDbSet<Evaluation> ScoutReports { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public FootballLegendsDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in FootballLegendsDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of FootballLegendsDbContext since ABP automatically handles it.
         */
        public FootballLegendsDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }
    }
}

﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace FootballLegends.EntityFramework.Repositories
{
    public abstract class FootballLegendsRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<FootballLegendsDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected FootballLegendsRepositoryBase(IDbContextProvider<FootballLegendsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class FootballLegendsRepositoryBase<TEntity> : FootballLegendsRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected FootballLegendsRepositoryBase(IDbContextProvider<FootballLegendsDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}

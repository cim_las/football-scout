using FootballLegends.Migrations.Data;
using FootballLegends.People;

namespace FootballLegends.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FootballLegends.EntityFramework.FootballLegendsDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "FootballLegends";
        }

        protected override void Seed(FootballLegends.EntityFramework.FootballLegendsDbContext context)
        {
            // This method will be called every time after migrating to the latest version.
            // You can add any seed data here...

            //context.People.AddOrUpdate(
            //    p => p.Name,
            //    new Person { Name = "Isaac Asimov" },
            //    new Person { Name = "Thomas More" },
            //    new Person { Name = "George Orwell" },
            //    new Person { Name = "Douglas Adams" }
            //    );

            new InitialDataBuilder().Build(context);
        }
    }
}

﻿(function () {
    'use strict';

    var app = angular.module('app', [
        'ngAnimate',
        'ngSanitize',

        'ui.router',
        'ui.bootstrap',
        'ui.jq',

        'abp'
    ]);

    //Configuration for Angular UI routing.
    app.config([
        '$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');
            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: '/App/Main/views/home/home.cshtml',
                    menu: 'Home' //Matches to name of 'Home' menu in FootballLegendsNavigationProvider
                })
                .state('personlist', {
                    url: '/list',
                    templateUrl: '/App/Main/views/person/list.cshtml',
                    menu: 'PersonList' //Matches to name of 'PersonList' menu in FootballLegendsNavigationProvider
                })
                .state('scoutDetail', {
                    url: '/detail',
                    templateUrl: '/App/Main/views/scout/detail.cshtml',
                    menu: 'ScoutDetail' //Matches to name of 'PersonList' menu in FootballLegendsNavigationProvider
                })
                .state('users', {
                    url: '/users',
                    templateUrl: abp.appPath + 'App/Main/views/users/index.cshtml',
                    menu: 'Users' //Matches to name of 'Users' menu in ModuleZeroSampleProjectNavigationProvider
                })
                .state('about', {
                    url: '/about',
                    templateUrl: '/App/Main/views/about/about.cshtml',
                    menu: 'About' //Matches to name of 'About' menu in FootballLegendsNavigationProvider
                });
        }
    ]);
})();
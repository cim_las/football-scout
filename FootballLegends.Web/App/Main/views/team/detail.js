﻿(function () {
    var app = angular.module('app');

    var controllerId = 'fl.views.team.detail';
    app.controller(controllerId, [
        '$scope', 'abp.services.footballlegends.team',
        function ($scope, teamService) {
            var vm = this;

            vm.localize = abp.localization.getSource('FootballLegends');

            vm.team = null;
            vm.userId = null;

            //$scope.selectedTaskState = 0;

            //$scope.$watch('selectedTaskState', function (value) {
            //    vm.refreshTasks();
            //});

            vm.createTeam = function () {
                abp.ui.setBusy( //Set whole page busy until getTasks complete
                    null,
                    teamService.createTeam({
                        pUserId: vm.userId
                        //Call application service method directly from javascript
                    }).success(function(data) {
                        vm.team = data.team;
                    })
                );
            };
        }
    ]);
})();
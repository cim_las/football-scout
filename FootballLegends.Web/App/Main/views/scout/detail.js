﻿(function () {
    var app = angular.module('app');

    var controllerId = 'fl.views.scout.detail';
    app.controller(controllerId, [
        '$scope', 'abp.services.footballlegends.evaluation',
        function ($scope, evaluationService) {
            var vm = this;

            vm.localize = abp.localization.getSource('FootballLegends');

            vm.evaluation = null;
            vm.person = null;

            //$scope.selectedTaskState = 0;

            //$scope.$watch('selectedTaskState', function (value) {
            //    vm.refreshTasks();
            //});

            vm.scoutPerson = function () {
                abp.ui.setBusy( //Set whole page busy until getTasks complete
                    null,
                    evaluationService.getEvaluation({ //Call application service method directly from javascript
                    }).success(function (data) {
                        vm.evaluation = data.evaluation;
                        vm.person = data.person;
                    })
                );
            };
        }
    ]);
})();
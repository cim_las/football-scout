﻿(function () {
    var app = angular.module('app');

    var controllerId = 'fl.views.person.detail';
    app.controller(controllerId, [
        '$scope', 'abp.services.footballlegends.person',
        function ($scope, personService) {
            var vm = this;

            vm.localize = abp.localization.getSource('FootballLegends');

            vm.person = null;

            //$scope.selectedTaskState = 0;

            //$scope.$watch('selectedTaskState', function (value) {
            //    vm.refreshTasks();
            //});

            vm.scoutPerson = function () {
                abp.ui.setBusy( //Set whole page busy until getTasks complete
                    null,
                    personService.getPerson({ //Call application service method directly from javascript
                    }).success(function (data) {
                        vm.person = data.person;
                    })
                );
            };
        }
    ]);
})();
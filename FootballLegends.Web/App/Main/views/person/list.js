﻿(function () {
    var app = angular.module('app');

    var controllerId = 'fl.views.person.list';
    app.controller(controllerId, [
        '$scope', 'abp.services.footballlegends.person',
        function ($scope, personService) {
            var vm = this;

            vm.localize = abp.localization.getSource('FootballLegends');

            vm.people = [];

            //$scope.selectedTaskState = 0;

            //$scope.$watch('selectedTaskState', function (value) {
            //    vm.refreshTasks();
            //});

            vm.refreshPeople = function () {
                abp.ui.setBusy( //Set whole page busy until getTasks complete
                    null,
                    personService.getAllPeople({ //Call application service method directly from javascript
                        //state: $scope.selectedTaskState > 0 ? $scope.selectedTaskState : null
                    }).success(function (data) {
                        vm.people = data.people;
                    })
                );
            };

            //vm.changeTaskState = function (task) {
            //    var newState;
            //    if (task.state == 1) {
            //        newState = 2; //Completed
            //    } else {
            //        newState = 1; //Active
            //    }

            //    taskService.updateTask({
            //        taskId: task.id,
            //        state: newState
            //    }).success(function () {
            //        task.state = newState;
            //        abp.notify.info(vm.localize('TaskUpdatedMessage'));
            //    });
            //};

            vm.getPersonCountText = function () {
                return abp.utils.formatString(vm.localize('Xpeople'), vm.people.length);
            };
        }
    ]);
})();
﻿using Abp.Web.Mvc.Views;

namespace FootballLegends.Web.Views
{
    public abstract class FootballLegendsWebViewPageBase : FootballLegendsWebViewPageBase<dynamic>
    {

    }

    public abstract class FootballLegendsWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected FootballLegendsWebViewPageBase()
        {
            LocalizationSourceName = FootballLegendsConsts.LocalizationSourceName;
        }
    }
}
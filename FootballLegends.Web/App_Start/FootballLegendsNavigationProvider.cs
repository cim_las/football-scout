﻿using Abp.Application.Navigation;
using Abp.Localization;

namespace FootballLegends.Web
{
    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class FootballLegendsNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        "Home",
                        new LocalizableString("HomePage", FootballLegendsConsts.LocalizationSourceName),
                        url: "#/",
                        icon: "fa fa-home"
                        )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "PersonList",
                        new LocalizableString("PersonList", FootballLegendsConsts.LocalizationSourceName),
                        url: "#/list",
                        icon: "fa fa-info"
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "ScoutDetail",
                        new LocalizableString("ScoutDetail", FootballLegendsConsts.LocalizationSourceName),
                        url: "#/detail",
                        icon: "fa fa-info"
                        )
                ).AddItem(
                   new MenuItemDefinition(
                        "Users",
                        new LocalizableString("Users", FootballLegendsConsts.LocalizationSourceName),
                        url: "#/users",
                        icon: "fa fa-users"
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "About",
                        new LocalizableString("About", FootballLegendsConsts.LocalizationSourceName),
                        url: "#/about",
                        icon: "fa fa-info"
                        )
                );
        }
    }
}

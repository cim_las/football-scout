﻿using System;
using System.Globalization;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Security;
using Abp.UI;
using Abp.Web.Mvc.Models;
using Abp.Zero.Configuration;
using FootballLegends.MultiTenancy;
using FootballLegends.Users;
using FootballLegends.Web.Models.Account;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace FootballLegends.Web.Controllers
{
    [Authorize]
    public class HomeController : FootballLegendsControllerBase
    {
        public ActionResult Index()
        {
            return View("~/App/Main/views/layout/layout.cshtml"); //Layout of the angular application.
        }
	}
}
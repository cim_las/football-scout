﻿using Abp.Web.Mvc.Controllers;

namespace FootballLegends.Web.Controllers
{
    /// <summary>
    /// Derive all Controllers from this class.
    /// </summary>
    public abstract class FootballLegendsControllerBase : AbpController
    {
        protected FootballLegendsControllerBase()
        {
            LocalizationSourceName = FootballLegendsConsts.LocalizationSourceName;
        }
    }
}